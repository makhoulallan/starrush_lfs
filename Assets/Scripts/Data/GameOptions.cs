﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOptions
{
    public bool controlInvertHorizontal;
    public bool controlInvertVertical;
    public bool controlMovingReticle;
    public float musicVolume;
    public float fxVolume;

    const string invHor = "ControlInvertHorizontal";
    const string invVer = "ControlInvertVertical";
    const string movRet = "MovingReticle";
    const string musVol = "MusicVolume";
    const string fxVol = "FxVolume";

    public GameOptions()
    {
        if (PlayerPrefs.HasKey(invHor))
        {
            controlInvertHorizontal = PlayerPrefs.GetInt(invHor) == 1;
        }
        else
        {
            controlInvertHorizontal = false;
            PlayerPrefs.SetInt(invHor, controlInvertHorizontal ? 1 : 0);
        }

        if (PlayerPrefs.HasKey(invVer))
        {
            controlInvertVertical = PlayerPrefs.GetInt(invVer) == 1;
        }
        else
        {
            controlInvertVertical = false;
            PlayerPrefs.SetInt(invVer, controlInvertVertical ? 1 : 0);
        }

        if (PlayerPrefs.HasKey(movRet))
        {
            controlMovingReticle = PlayerPrefs.GetInt(movRet) == 1;
        }
        else
        {
            controlMovingReticle = false;
            PlayerPrefs.SetInt(movRet, controlMovingReticle ? 1 : 0);
        }

        if (PlayerPrefs.HasKey(musVol))
        {
            musicVolume = PlayerPrefs.GetFloat(musVol);
        }
        else
        {
            musicVolume = 1.0f;
            PlayerPrefs.SetFloat(musVol, musicVolume);
        }

        if (PlayerPrefs.HasKey(fxVol))
        {
            fxVolume = PlayerPrefs.GetFloat(fxVol);
        }
        else
        {
            fxVolume = 1.0f;
            PlayerPrefs.SetFloat(fxVol, fxVolume);
        }
    }

    public void LoadVariables()
    {
        controlInvertHorizontal = PlayerPrefs.GetInt(invHor) == 1;
        controlInvertVertical = PlayerPrefs.GetInt(invVer) == 1;
        controlMovingReticle = PlayerPrefs.GetInt(movRet) == 1;
        musicVolume = PlayerPrefs.GetFloat(musVol);
        fxVolume = PlayerPrefs.GetFloat(fxVol);
    }

    public void SaveVariables()
    {
        PlayerPrefs.SetInt(invHor, controlInvertHorizontal ? 1 : 0);
        PlayerPrefs.SetInt(invVer, controlInvertVertical ? 1 : 0);
        PlayerPrefs.SetInt(movRet, controlMovingReticle ? 1 : 0);
        PlayerPrefs.SetFloat(musVol, musicVolume);
        PlayerPrefs.SetFloat(fxVol, fxVolume);
    }
}
