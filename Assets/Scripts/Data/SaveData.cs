﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveData : MonoBehaviour
{
    public string folderName = "PlayTestData/";
    public string fileName = "paytestData.txt";
    string dataLocation;

    private void Awake()
    {
        if (!folderName.StartsWith("\\"))
            folderName = "\\" + folderName;
        if (!folderName.EndsWith("\\"))
            folderName += "\\";

        dataLocation = Application.dataPath + folderName;

        if (!Directory.Exists(dataLocation))
        {
            Directory.CreateDirectory(dataLocation);
        }

        if (!File.Exists(dataLocation + fileName))
        {
            File.WriteAllText(dataLocation + fileName, "");
        }
    }

    public void WriteDate(string data)
    {
        WriteOnFile(fileName, data);
    }

    void WriteOnFile(string fileToWrite, string data)
    {
        File.AppendAllText(dataLocation + fileToWrite, data + System.Environment.NewLine);
    }
}
