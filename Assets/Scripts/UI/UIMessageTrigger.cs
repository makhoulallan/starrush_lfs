﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMessageTrigger : MonoBehaviour
{
    GameManager gameManager;
    UIPlayer playerUI;
    [Multiline]
    public string message;    

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        playerUI = gameManager.uiManager.playerUi;
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            playerUI.ShowPlayerMessage(message);
    }
}
