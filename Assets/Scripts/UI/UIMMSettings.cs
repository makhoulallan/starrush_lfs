﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMMSettings : MonoBehaviour
{
    public Toggle invVertical;
    public Toggle invHorizontal;
    public Slider musicVolume;
    public Slider fxVolume;

    UIMainMenu uIMainMenu;
    GameOptions gameOptions;

    void Start()
    {
        uIMainMenu = gameObject.GetComponentInParent<UIMainMenu>();

        musicVolume.maxValue = 1.0f;
        fxVolume.maxValue = 1.0f;

        gameOptions = uIMainMenu.gameManager.gameOptions;
        gameOptions.LoadVariables();

        invVertical.isOn = gameOptions.controlInvertVertical;
        invHorizontal.isOn = gameOptions.controlInvertHorizontal;
        musicVolume.value = gameOptions.musicVolume;
        fxVolume.value = gameOptions.fxVolume;

        uIMainMenu.SetSelectedGameObject(invVertical.gameObject);
    }

    public void UpdateVertical()
    {
        gameOptions.controlInvertVertical = invVertical.isOn;
        SaveAndUpdate();
    }

    public void UpdateHorizontal()
    {
        gameOptions.controlInvertHorizontal = invHorizontal.isOn;
        SaveAndUpdate();
    }

    public void UpdateMusic()
    {
        gameOptions.musicVolume = musicVolume.value;
        SaveAndUpdate();
    }

    public void UpdateFx()
    {
        gameOptions.fxVolume = fxVolume.value;
        SaveAndUpdate();
    }

    void SaveAndUpdate()
    {
        gameOptions.SaveVariables();
        uIMainMenu.UpdateOptions();
    }
}
