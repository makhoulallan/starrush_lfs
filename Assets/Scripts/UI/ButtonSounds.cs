﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonSounds : MonoBehaviour, ISelectHandler, IPointerEnterHandler
{
    Button but;

    public AudioClip hoverSound;
    public AudioClip selectedSound;
    public AudioClip clickedSound;

    public AudioSource audioSource;
    [Range(0.0f, 1.0f)]
    public float audioVolume = 1.0f;

    private void Start()
    {
        but = GetComponent<Button>();
        but.onClick.AddListener(OnClickListener);
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (audioSource != null && selectedSound != null)
            audioSource.PlayOneShot(selectedSound, audioVolume);
    }

    void OnClickListener()
    {
        if (audioSource != null && clickedSound != null)
            audioSource.PlayOneShot(clickedSound, audioVolume);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (audioSource != null && hoverSound != null)
            audioSource.PlayOneShot(hoverSound, audioVolume);
    }
}
