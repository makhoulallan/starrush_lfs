﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISSGameplay : MonoBehaviour
{
    public float timeBetween = 1.0f;
    public Sprite[] images;

    int index = 0;

    public Image imageRend;
    bool counting = false;
    float timer = 0.0f;

    void Start()
    {        
        if (images.Length > 0)
        {
            SwitchImage();
            counting = true;
        }
    }

    void Update()
    {
        if(counting)
        {
            timer += Time.deltaTime;
            if(timer >= timeBetween)
            {
                counting = false;
                SwitchImage();
            }
        }
    }

    void SwitchImage()
    {
        index %= images.Length;
        Sprite img = images[index];

        imageRend.sprite = img;

        timer = 0.0f;
        counting = true;
        index++;
    }
}
