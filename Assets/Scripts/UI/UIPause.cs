﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class UIPause : MonoBehaviour
{
    public GameObject pauseButtonsPanel;
    public GameObject settingsPanel;
    public GameObject areYouSurePanel;

    [HideInInspector]public GameManager gameManager;
    EventSystem eventSystem;
    InputActionAsset inputActionAsset;
    private InputAction cancelAction;

    bool anyWindowOpen = false;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        eventSystem = EventSystem.current;

        inputActionAsset = gameManager.inputActionsAsset;
        cancelAction = inputActionAsset.FindAction("UI/Cancel");
        cancelAction.Enable();

        ShowPause();
    }

    private void Update()
    {
        if(cancelAction.triggered)
        {
            if(anyWindowOpen)
            {
                ShowPause();
                anyWindowOpen = false;
            }
            else
            {
                ResumeGame();
            }
        }
    }

    public void ResumeGame()
    {
        gameManager.PauseGame(false);
    }

    public void ShowSettings(bool value)
    {
        if(settingsPanel != null)
        {
            settingsPanel.SetActive(value);
            anyWindowOpen = value;
            SetButtonSelectable(settingsPanel);
            SetInteractableAllButtons(!value, pauseButtonsPanel);
            if (!value)
            {
                ShowPause();
                UpdateOptions();
            }
        }
    }

    public void ShowAreYouSure(bool value)
    {
        if(areYouSurePanel != null)
        {
            areYouSurePanel.SetActive(value);
            anyWindowOpen = value;
            SetButtonSelectable(areYouSurePanel);
            SetInteractableAllButtons(!value, pauseButtonsPanel);
            if (!value)
                ShowPause();
        }
    }

    public void ShowPause()
    {
        settingsPanel.SetActive(false);
        areYouSurePanel.SetActive(false);
        SetInteractableAllButtons(true, pauseButtonsPanel);
        SetButtonSelectable(pauseButtonsPanel);
    }

    public void QuitLevel()
    {        
        gameManager.LoadMainMenu();
    }

    void SetButtonSelectable(GameObject parent)
    {
        GameObject but = null;
        Button[] allButtons = parent.GetComponentsInChildren<Button>();
        foreach (var b in allButtons)
        {
            if (b.interactable)
            {
                but = b.gameObject;
                break;
            }
        }

        eventSystem.firstSelectedGameObject = but;
        eventSystem.SetSelectedGameObject(but);
        but.GetComponent<Selectable>().Select();
    }

    public void SetSelectedGameObject(GameObject sel)
    {
        eventSystem.firstSelectedGameObject = sel;
        eventSystem.SetSelectedGameObject(sel);
        sel.GetComponent<Selectable>().Select();
    }

    void SetInteractableAllButtons(bool value, GameObject parent)
    {
        Button[] allButtons = parent.GetComponentsInChildren<Button>();
        foreach (var b in allButtons)
        {
            b.interactable = value;
        }
    }

    public void UpdateOptions()
    {
        gameManager.UpdateOptions();
    }
}
