﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class UIPlayer : MonoBehaviour
{
    public Slider playerHealth;    
    public GameObject youDiedPanel;
    public GameObject youWonPanel;
    public GameObject youFailedPanel;
    public UIMessage playerMessage;
    public Slider strafeBar;
    public Slider boostBar;
    public Image countdownImage;
    public Sprite[] countdownSprites;

    public Slider shootSlider;
    bool shotSliderVisible = true;

    public TextMeshProUGUI currentHostageAmount;
    public TextMeshProUGUI maxHostageAmount;
    public TextMeshProUGUI goalHostageAmount;

    [HideInInspector]public GameManager gameManager;
    EventSystem eventSystem;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        eventSystem = EventSystem.current;

        countdownImage.sprite = countdownSprites[0];

        if(shootSlider != null)
            shotSliderVisible = shootSlider.gameObject.activeInHierarchy;
    }

    public bool SetPlayerMaxHealthUI(int value)
    {
        if (playerHealth != null)
        {
            playerHealth.maxValue = value;
            return true;
        }
        return false;
    }

    public bool SetPlayerHealthUI(int value)
    {
        if (playerHealth != null)
        {
            playerHealth.value = value;
            return true;
        }
        return false;
    }

    public bool SetMaxStrafe(float value)
    {
        if (strafeBar != null)
        {
            strafeBar.maxValue = value;
            return true;
        }
        return false;
    }

    public bool SetStrafeBar(float value)
    {
        if (strafeBar != null)
        {
            strafeBar.value = value;
            return true;
        }
        return false;
    }

    public bool SetMaxBoost(float value)
    {
        if (boostBar != null)
        {
            boostBar.maxValue = value;
            return true;
        }
        return false;
    }

    public bool SetBoostBar(float value)
    {
        if (boostBar != null)
        {
            boostBar.value = value;
            return true;
        }
        return false;
    }

    public bool SetCurrentHostageAmount(int value)
    {
        if (currentHostageAmount != null)
        {
            currentHostageAmount.text = value.ToString();
            return true;
        }
        return false;
    }

    public bool SetMaxHostageAmount(int value)
    {
        if (maxHostageAmount != null)
        {
            maxHostageAmount.text = value.ToString();
            return true;
        }
        return false;
    }

    public bool SetGoalHostageAmount(int value)
    {
        if (goalHostageAmount != null)
        {
            goalHostageAmount.text = "Goal: " + value;
            return true;
        }
        return false;
    }

    public bool ShowYouDiedPanel(bool value)
    {
        if (youDiedPanel != null)
        {
            youDiedPanel.SetActive(value);
            SetButtonSelectable(youDiedPanel);
            return true;
        }
        return false;
    }

    public bool ShowYouWonPanel(bool value)
    {
        if (youWonPanel != null)
        {
            youWonPanel.SetActive(value);
            SetButtonSelectable(youWonPanel);
            return true;
        }
        return false;
    }

    public bool ShowYouFailedPanel(bool value)
    {
        if (youFailedPanel != null)
        {
            youFailedPanel.SetActive(value);
            SetButtonSelectable(youFailedPanel);
            return true;
        }
        return false;
    }

    public void QuitLevel()
    {
        gameManager.LoadMainMenu();
    }

    public void QuitGame()
    {
        gameManager.QuitGame();
    }

    public void PlayAgain()
    {
        ShowYouWonPanel(false);
        ShowYouFailedPanel(false);
        gameManager.ResetPlayer();
    }

    public void PlayNextLevel()
    {
        ShowYouWonPanel(false);
        ShowYouFailedPanel(false);
        gameManager.LoadNextLevel();
    }

    void SetButtonSelectable(GameObject parent)
    {
        GameObject but = null;
        Button[] allButtons = parent.GetComponentsInChildren<Button>();
        foreach (var b in allButtons)
        {
            if (b.interactable)
            {
                but = b.gameObject;
                break;
            }
        }

        eventSystem.firstSelectedGameObject = but;
        eventSystem.SetSelectedGameObject(but);
        but.GetComponent<Selectable>().Select();
    }

    public void SetSelectedGameObject(GameObject sel)
    {
        eventSystem.firstSelectedGameObject = sel;
        eventSystem.SetSelectedGameObject(sel);
        sel.GetComponent<Selectable>().Select();
    }

    public void ShowPlayerMessage(string message)
    {
        if (playerMessage != null)
        {
            playerMessage.gameObject.SetActive(true);
            playerMessage.messageToShow = message;
            playerMessage.ShowText();
        }
    }

    public void HidePlayerMessage()
    {
        if(playerMessage != null)
        {
            playerMessage.gameObject.SetActive(false);
        }
    }

    //num = 1, 2 or 3 --> same sprite
    public void SetCountdownImage(int num)
    {
        Sprite spriteToShow = countdownSprites[0];
        switch (num)
        {
            case 1:
                spriteToShow = countdownSprites[2];
                break;
            case 2:
                spriteToShow = countdownSprites[1];
                break;
            case 3:
                spriteToShow = countdownSprites[0];
                break;
        }

        countdownImage.sprite = spriteToShow;
    }

    public void ShowCountdownImage(bool value)
    {
        if(countdownImage != null)
        {
            countdownImage.gameObject.SetActive(value);
        }
    }

    public void SetShotSliderValue(float value)
    {
        if(shootSlider != null)
        {
            shootSlider.value = value;
        }
    }

    public void SetShotSliderVisible(bool value)
    {
        if (value != shotSliderVisible)
        {
            if (shootSlider != null)
            {
                shootSlider.gameObject.SetActive(value);
                shotSliderVisible = value;
            }
        }
    }
}
