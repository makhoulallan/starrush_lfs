﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public UIPlayer playerUi;
    public UIPause pauseUi;
    public LaserReticle reticle;

    [Header("Button Sounds")]
    public AudioClip hoverSound;
    public AudioClip selectedSound;
    public AudioClip clickedSound;

    AudioSource audioSource;
    [Range(0.0f, 1.0f)]
    public float audioVolume = 1.0f;

    private void Start()
    {
        audioSource = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().audioManager.fxSource;
        AddButtonSounds();
    }

    public bool SetPlayerMaxHealthUI(int value)
    {
        return playerUi.SetPlayerMaxHealthUI(value);
    }

    public bool SetPlayerHealthUI(int value)
    {
        return playerUi.SetPlayerHealthUI(value);
    }

    public bool SetMaxStrafe(float value)
    {
        return playerUi.SetMaxStrafe(value);
    }

    public bool SetStrafeBar(float value)
    {
        return playerUi.SetStrafeBar(value);
    }

    public bool SetMaxBoost(float value)
    {
        return playerUi.SetMaxBoost(value);
    }

    public bool SetBoostBar(float value)
    {
        return playerUi.SetBoostBar(value);
    }

    public bool SetCurrentHostageAmount(int value)
    {
        return playerUi.SetCurrentHostageAmount(value);
    }

    public bool SetMaxHostageAmount(int value)
    {
        return playerUi.SetMaxHostageAmount(value);
    }

    public bool SetGoalHostageAmount(int value)
    {
        return playerUi.SetGoalHostageAmount(value);
    }

    public void PauseGame(bool value)
    {
        reticle.gameObject.SetActive(!value);
        if(!value)
        {
            pauseUi.ShowPause();
        }
        pauseUi.gameObject.SetActive(value);        
    }

    public void ShowYouDied(bool value)
    {
        reticle.gameObject.SetActive(!value);
        playerUi.ShowYouDiedPanel(value);
    }

    public void ShowYouWon(bool value)
    {
        reticle.gameObject.SetActive(!value);
        playerUi.ShowYouWonPanel(value);
    }

    public void ShowYouFailed(bool value)
    {
        reticle.gameObject.SetActive(!value);
        playerUi.ShowYouFailedPanel(value);
    }

    public void SetCountdownImage(int num)
    {
        playerUi.SetCountdownImage(num);
    }

    public void ShowCountdownImage(bool value)
    {
        playerUi.ShowCountdownImage(value);
    }

    public void SetShootSlider(float value)
    {
        playerUi.SetShotSliderValue(value);
    }

    public void SetShotSliderVisible(bool value)
    {
        playerUi.SetShotSliderVisible(value);
    }

    void AddButtonSounds()
    {
        Button[] allBt = transform.GetComponentsInChildren<Button>(true);
        foreach (var bt in allBt)
        {
            ButtonSounds bS = bt.GetComponent<ButtonSounds>();
            if (bS == null)
            {
                bS = bt.gameObject.AddComponent<ButtonSounds>();
            }
            bS.hoverSound = hoverSound;
            bS.selectedSound = selectedSound;
            bS.clickedSound = clickedSound;
            bS.audioSource = audioSource;
            bS.audioVolume = audioVolume;
        }
    }
}