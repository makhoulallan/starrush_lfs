﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;

public class UIMessage : MonoBehaviour
{
    public TextMeshProUGUI messageText;
    public string messageToShow;
    [Range(0.0f, 5.0f)]
    public float timeDelay = 0.5f;
    [Range(0.0f, 0.5f)]
    public float timeInterval = 0.1f;
    [Range(0.0f, 5.0f)]
    public float timeToShow = 2.0f;

    string tempText = "";
    int indexCounter = 0;

    bool isShowing = false;
    bool skipped = false;

    bool previousMessage = false;

    UIPlayer uIPlayer;
    InputActionAsset inputActionAsset;
    private InputAction cancelAction;

    void Awake()
    {
        uIPlayer = GetComponentInParent<UIPlayer>();
        inputActionAsset = uIPlayer.gameManager.inputActionsAsset;
        cancelAction = inputActionAsset.FindAction("UI/Cancel");
        cancelAction.Enable();
    }

    private void Update()
    {
        if (cancelAction.triggered && isShowing)
            Skip();
    }

    [ContextMenu("Skip Message")]
    public void Skip()
    {
        skipped = true;
        StopCoroutine(WaitShow());
        tempText = messageToShow;
        UpdateText();
        StartCoroutine(WaitAfter());
    }

    public void ShowText()
    {
        tempText = "";
        indexCounter = 1;

        previousMessage = true;

        StopCoroutine(WaitShow());
        StopCoroutine(WaitAfter());

        StartCoroutine(WaitDelay());
    }

    IEnumerator WaitDelay()
    {
        isShowing = false;
        ResetText();

        StopCoroutine(WaitShow());
        StopCoroutine(WaitAfter());

        yield return new WaitForSecondsRealtime(timeDelay);

        if (timeInterval > 0)
        {
            tempText = messageToShow.Substring(0, indexCounter);
            UpdateText();            
            StartCoroutine(WaitShow());
        }
        else
            Skip();
    }

    IEnumerator WaitShow()
    {
        isShowing = true;        
        yield return new WaitForSecondsRealtime(timeInterval);
        if (!skipped)
        {
            indexCounter++;
            if (indexCounter <= messageToShow.Length)
            {
                tempText = messageToShow.Substring(0, indexCounter);
                UpdateText();
                StartCoroutine(WaitShow());
            }
            else
            {
                previousMessage = false;
                StartCoroutine(WaitAfter());
            }
        }
    }

    IEnumerator WaitAfter()
    {
        isShowing = false;
        yield return new WaitForSecondsRealtime(timeToShow);
        if (!previousMessage)
        {
            ResetText();
            uIPlayer.HidePlayerMessage();
        }
    }

    void UpdateText()
    {
        messageText.text = tempText;
    }

    public void ResetText()
    {
        tempText = "";
        UpdateText();
    }
}