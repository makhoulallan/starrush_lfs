﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using TMPro;

public class UIMainMenu : MonoBehaviour
{
    public GameObject titlePanel;
    public GameObject mainMenuPanel;
    public GameObject levelSelectionPanel;
    public GameObject creditsPanel;
    public GameObject areYouSurePanel;
    public GameObject optionsPanel;

    bool titleActive = true;

    public bool showTitleFirst = true;
    
    [HideInInspector] public GameManager gameManager;
    MainMenuTitle mainMenuTitle;
    EventSystem eventSystem;
    InputActionAsset inputActionAsset;
    private InputAction cancelAction;

    [Header("Button Sounds")]
    public AudioClip hoverSound;
    public AudioClip selectedSound;
    public AudioClip clickedSound;

    public AudioSource audioSource;
    [Range(0.0f, 1.0f)]
    public float audioVolume = 1.0f;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        mainMenuTitle = GameObject.FindGameObjectWithTag("MainMenu").GetComponent<MainMenuTitle>();

        showTitleFirst = mainMenuTitle.showMainMenuFirst;
        mainMenuTitle.showMainMenuFirst = false;

        eventSystem = EventSystem.current;

        inputActionAsset = gameManager.inputActionsAsset;
        cancelAction = inputActionAsset.FindAction("UI/Cancel");
        cancelAction.Enable();

        AddButtonSounds();

        if (showTitleFirst)
            ShowTitle();
        else if (mainMenuTitle.showCredits)
            ShowCredits();
        else
            ShowMainMenu();
    }

    void Update()
    {
        if (titleActive)
        {
            if (Input.anyKey)
            {
                ShowMainMenu();
                titleActive = false;
            }
        }

        if (cancelAction.triggered)
        {
            ShowMainMenu();
        }
    }

    public void StartGame()
    {
        gameManager.StartGame();
    }

    public void StartLevel(string levelName)
    {
        gameManager.StartGame(levelName);
    }

    public void QuitGame()
    {
        gameManager.QuitGame();
    }

    public void ShowTitle()
    {
        if (titlePanel != null)
        {
            titlePanel.SetActive(true);
            mainMenuPanel.SetActive(false);
            levelSelectionPanel.SetActive(false);
            creditsPanel.SetActive(false);
            areYouSurePanel.SetActive(false);
            optionsPanel.SetActive(false);
            titleActive = true;
        }
    }

    public void ShowMainMenu()
    {
        if (mainMenuPanel != null)
        {
            titlePanel.SetActive(false);
            mainMenuPanel.SetActive(true);
            levelSelectionPanel.SetActive(false);
            creditsPanel.SetActive(false);
            areYouSurePanel.SetActive(false);
            optionsPanel.SetActive(false);

            SetButtonSelectable(mainMenuPanel);
        }
    }

    public void ShowLevelSelection()
    {
        if (levelSelectionPanel != null)
        {
            titlePanel.SetActive(false);
            mainMenuPanel.SetActive(false);
            levelSelectionPanel.SetActive(true);
            creditsPanel.SetActive(false);
            areYouSurePanel.SetActive(false);
            optionsPanel.SetActive(false);

            SetButtonSelectable(levelSelectionPanel);
        }
    }

    public void ShowCredits()
    {
        mainMenuTitle.showCredits = false;

        if (creditsPanel != null)
        {
            titlePanel.SetActive(false);
            mainMenuPanel.SetActive(false);
            levelSelectionPanel.SetActive(false);
            creditsPanel.SetActive(true);
            areYouSurePanel.SetActive(false);
            optionsPanel.SetActive(false);
            
            SetButtonSelectable(creditsPanel);
        }
    }

    public void ShowAreYouSure()
    {
        if (areYouSurePanel != null)
        {
            titlePanel.SetActive(false);
            mainMenuPanel.SetActive(false);
            levelSelectionPanel.SetActive(false);
            creditsPanel.SetActive(false);
            areYouSurePanel.SetActive(true);
            optionsPanel.SetActive(false);

            SetButtonSelectable(areYouSurePanel);
        }
    }

    public void ShowOptions()
    {
        if (optionsPanel != null)
        {
            titlePanel.SetActive(false);
            mainMenuPanel.SetActive(false);
            levelSelectionPanel.SetActive(false);
            creditsPanel.SetActive(false);
            areYouSurePanel.SetActive(false);
            optionsPanel.SetActive(true);

            SetButtonSelectable(optionsPanel);
        }
    }

    public void OptionsSensitivity()
    {

    }
    public void OptionsGraphics()
    {

    }
    public void OptionsControls()
    {

    }
    public void OptionsFOV()
    {

    }

    void SetButtonSelectable(GameObject parent)
    {
        GameObject but = null;
        Button[] allButtons = parent.GetComponentsInChildren<Button>();
        foreach (var b in allButtons)
        {
            if (b.interactable)
            {
                but = b.gameObject;
                break;
            }
        }

        eventSystem.firstSelectedGameObject = but;
        eventSystem.SetSelectedGameObject(but);
    }

    public void SetSelectedGameObject(GameObject sel)
    {
        eventSystem.firstSelectedGameObject = sel;
        eventSystem.SetSelectedGameObject(sel);
        sel.GetComponent<Selectable>().Select();
    }

    public void UpdateOptions()
    {
        gameManager.UpdateOptions();
    }

    void AddButtonSounds()
    {
        Button[] allBt = transform.GetComponentsInChildren<Button>(true);
        foreach (var bt in allBt)
        {
            ButtonSounds bS = bt.GetComponent<ButtonSounds>();
            if (bS == null)
            {
                bS = bt.gameObject.AddComponent<ButtonSounds>();
            }
            bS.hoverSound = hoverSound;
            bS.selectedSound = selectedSound;
            bS.clickedSound = clickedSound;
            bS.audioSource = audioSource;
            bS.audioVolume = audioVolume;
        }
    }
}
