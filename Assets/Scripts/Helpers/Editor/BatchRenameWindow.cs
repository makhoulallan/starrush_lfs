﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BatchRenameWindow : EditorWindow
{
    
    string baseName = "baseName_";
    int startNum = 0;

[MenuItem ("Tools/Rename Batch")]
    public static void  ShowWindow () {
        EditorWindow w = EditorWindow.GetWindow(typeof(BatchRenameWindow), false, "Batch Rename");
    }

     void OnGUI()
    {
        baseName = EditorGUILayout.TextField("Base name", baseName);
        startNum = EditorGUILayout.IntField("Start number", startNum);

        if(GUILayout.Button("Rename All Selected"))
        {
            Object[] allObj = Selection.objects;

            int index = startNum;

            Undo.RecordObjects(allObj, "Batch Rename");

            foreach(var ob in allObj)
            {
                GameObject gO = (GameObject)ob;
                ob.name = baseName + index.ToString("000");
                index++;
            }
        }
    }
}