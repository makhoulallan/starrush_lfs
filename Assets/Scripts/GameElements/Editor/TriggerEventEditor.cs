﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TriggerEvent))]
public class TriggerEventEditor : Editor
{
    TriggerEvent myScript;

    SerializedProperty m_OnEnter, m_OnStay, m_OnExit;
    

    private void OnEnable()
    {
        myScript = (TriggerEvent)target;
        m_OnEnter = serializedObject.FindProperty("onEnter");
        m_OnStay = serializedObject.FindProperty("onStay");
        m_OnExit = serializedObject.FindProperty("onExit");
    }

    public override void OnInspectorGUI()
    {
        myScript.triggerTag = EditorGUILayout.TextField("Trigger Tag", myScript.triggerTag);

        myScript.enableOnEnter = EditorGUILayout.Toggle("Enable On Enter", myScript.enableOnEnter);
        if (myScript.enableOnEnter)
        {
            EditorGUILayout.PropertyField(m_OnEnter, new GUIContent("OnEnter"));
        }

        myScript.enableOnStay = EditorGUILayout.Toggle("Enable On Stay", myScript.enableOnStay);
        if (myScript.enableOnStay)
        {
            EditorGUILayout.PropertyField(m_OnStay, new GUIContent("OnEnter"));
        }

        myScript.enableOnExit = EditorGUILayout.Toggle("Enable On Exit", myScript.enableOnExit);
        if (myScript.enableOnExit)
        {
            EditorGUILayout.PropertyField(m_OnExit, new GUIContent("OnEnter"));
        }

        serializedObject.ApplyModifiedProperties();
    }
}
