﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(HostageManager))]
public class HostageManagerEditor : Editor
{
    HostageManager myScript;
    int amountInScene = 0;

    private void Awake()
    {
        myScript = (HostageManager)target;
        amountInScene = GetAmountInScene();
    }

    public override void OnInspectorGUI()
    {
        if (amountInScene == 0)
            amountInScene = GetAmountInScene();

        EditorGUILayout.LabelField("Hostages in Scene: "+ amountInScene);
        EditorGUILayout.LabelField("Current Picked: " + myScript.currentHostage);
        myScript.necessaryHostageNum = EditorGUILayout.IntSlider("Amount Required", myScript.necessaryHostageNum,
            0, amountInScene);

        if(GUI.changed)
        {
            EditorUtility.SetDirty(myScript);
        }
    }

    int GetAmountInScene()
    {
        int num = 0;
        GameObject[] allPick = GameObject.FindGameObjectsWithTag("Pickable");
        foreach(var gO in allPick)
        {
            Pickable pick = gO.GetComponent<Pickable>();
            if(pick!=null)
            {
                if (pick.category == PickableClass.Hostage)
                    num++;
            }
        }
        return num;
    }
}
