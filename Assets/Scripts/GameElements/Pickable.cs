﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PickableClass { Hostage, Health};

public class Pickable : MonoBehaviour
{
    public PickableClass category = PickableClass.Hostage;
    public int amount = 1;
    public GameObject pickFx;
    public bool autoDestroyFx = true;
    public float destroyFxTime = 1.0f;
    List<GameObject> allChild = new List<GameObject>();

    [Header("Audio")]
    public AudioClip pickAudio;
    [Range(0.0f,1.0f)]
    public float pickVolume = 1.0f;

    [Header("Debug")]
    public bool pickTrigger = false;
    public bool resetTrigger = false;

    GameManager gameManager;
    Transform cameraTransform;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        cameraTransform = Camera.main.gameObject.transform;

        for (int i = 0; i < transform.childCount; i++)
        {
            allChild.Add(transform.GetChild(i).gameObject);
        }
    }

    private void Update()
    {
        if(pickTrigger)
        {
            pickTrigger = false;
            Pick(null);
        }

        if(resetTrigger)
        {
            resetTrigger = false;
            ResetPickable();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            Pick(other.gameObject);
        }
    }

    void Pick(GameObject gO)
    {
        switch (category)
        {
            case PickableClass.Health:
                if (gO != null)
                {
                    ShipUnit sU = gO.GetComponentInParent<ShipUnit>();
                    sU.Heal(amount);
                }
                DestroySelf();
                break;
            case PickableClass.Hostage:
                GameManager gM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
                HostageManager hM = gM.hostageManager;
                hM.RescueHostage(amount);
                DestroySelf();
                break;
        }

        gameManager.audioManager.PlayOneShot(pickAudio, pickVolume);
    }

    void DestroySelf()
    {
        if(pickFx != null)
        {
            GameObject fx = Instantiate(pickFx, cameraTransform);
            //fx.transform.localPosition = Vector3.zero;
            if(autoDestroyFx)
                Destroy(fx, destroyFxTime);
        }

        SetAllChildActive(false);
        GetComponentInChildren<Collider>().enabled = false;
    }

    public void ResetPickable()
    {
        SetAllChildActive(true);
        GetComponentInChildren<Collider>().enabled = true;
    }

    void SetAllChildActive(bool value)
    {
        foreach(var go in allChild)
        {
            go.SetActive(value);
        }
    }
}
