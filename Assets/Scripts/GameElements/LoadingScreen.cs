﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    public AsyncOperation loadingOperation;
    public AsyncOperation unloadingPreviousScreen;
    public Slider progressSlider;
    public TextMeshProUGUI progressText;

    void Start()
    {
        progressSlider.value = 0;
        progressText.text = "0%";
    }

    void Update()
    {
        if (loadingOperation != null)
        {
            progressSlider.value = loadingOperation.progress;
            progressText.text = (loadingOperation.progress * 100).ToString("00") + "%";

            if (loadingOperation.isDone)
            {
                Destroy(gameObject);
            }
        }
    }
}
