﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private InputAction startAction;
    public InputActionAsset inputActionsAsset;

    public UIManager uiManager;
    public AudioManager audioManager;
    public HostageManager hostageManager;
    public ShipController player;
    [HideInInspector]public GameOptions gameOptions;

    Vector3 levelStartPosition;
    Quaternion levelStartRotation;

    public GameObject loadingScreen;
    LoadingScreen loadingScreenCreated;

    public bool enablePause = true;
    public bool pauseMusicOnGamePause = true;
    public string mainMenuLevel = "MainMenu";
    public string[] allLevels;
    [Tooltip("Current Level Number from 0 to max-1")]
    public int currentLevel = 0;

    private bool isPaused = false;
    public bool IsPaused
    {
        get { return isPaused; }
        set
        {
            if(isPaused != value)
            {
                PauseGame(value);
            }
            isPaused = value;
        }
    }
    [Header("Countdown")]
    int countdown = 3;
    public AudioClip countdownAudio;
    public AudioClip countdownFinishedAudio;
    [Range(0.0f,1.0f)]
    public float countdownVolume = 1.0f;

    void Start()
    {
        startAction = inputActionsAsset.FindAction("Navigation/Start");

        startAction.Enable();

        gameOptions = new GameOptions();
        UpdateOptions();

        if (player != null)
        {
            levelStartPosition = player.transform.position;
            levelStartRotation = player.transform.rotation;
        }

        if(uiManager != null)
            StartCountdown();
    }

    void Update()
    {
        if (startAction.triggered)
        {
            if(enablePause)
                PauseGame(!IsPaused);
        }
    }

    void StartCountdown()
    {
        if (countdown > 0)
        {
            uiManager.ShowCountdownImage(true);
            uiManager.SetCountdownImage(countdown);
            audioManager.PlayOneShot(countdownAudio, countdownVolume);
            StartCoroutine(CountdownTimer());
        }
        else
        {
            audioManager.PlayOneShot(countdownFinishedAudio, countdownVolume);
            uiManager.ShowCountdownImage(false);
            player.enableMovement = true;
        }
    }

    IEnumerator CountdownTimer()
    {
        yield return new WaitForSeconds(1);
        countdown--;
        StartCountdown();
    }

    public void StartGame(string levelName = "")
    {
        if (levelName == "")
            levelName = mainMenuLevel;

        LoadScene(levelName);
    }

    public void LoadNextLevel()
    {
        string levelToLoad;
        int levelIndex = currentLevel + 1;
        if (levelIndex == allLevels.Length)
        {
            levelToLoad = mainMenuLevel;
            GameObject.FindGameObjectWithTag("MainMenu").GetComponent<MainMenuTitle>().showCredits = true;
        }
        else
        {
            levelToLoad = allLevels[levelIndex];
        }
        PauseGame(false, true);
        LoadScene(levelToLoad);
    }

    public void LoadMainMenu()
    {
        if (IsPaused)
            PauseGame(false, true);
        Time.timeScale = 1;

        LoadScene("MainMenu");
    }

    public void QuitGame()
    {
            Application.Quit();
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#endif
    }

    public void PauseGame(bool paused, bool loadingLevel = false)
    {
        if (IsPaused != paused)
            isPaused = paused;

       // if (!paused && !isCountdown)
       //     player.enableMovement = false;

        float tScale = IsPaused ? 0.0f : 1.0f;

        Time.timeScale = tScale;
        if (pauseMusicOnGamePause)
            audioManager.PauseMusic(IsPaused);
        Cursor.visible = IsPaused;
        if(!loadingLevel)
            uiManager.PauseGame(IsPaused);
    }

    public void LoadScene(string sceneToLoad)
    {
        loadingScreenCreated = Instantiate(loadingScreen).GetComponent<LoadingScreen>();
        DontDestroyOnLoad(loadingScreenCreated.gameObject);
        loadingScreenCreated.loadingOperation = SceneManager.LoadSceneAsync(sceneToLoad);
    }

    public void PlayerDied(ShipController playerGO)
    {
        uiManager.ShowYouDied(true);
        player.enableMovement = false;
        enablePause = false;
        Time.timeScale = 0;
        player = playerGO;
    }

    public void ResetPlayer()
    {
        uiManager.ShowYouDied(false);
        Time.timeScale = 1;

        ResetAllTurns();
        player.ResetController();
        player.transform.position = levelStartPosition;
        player.transform.rotation = levelStartRotation;
        player.GetComponent<ShipUnit>().ResetUnit();        

        ResetGame();
    }

    ///Reset all elements in the scene to their initial state
    void ResetGame()
    {
        //Enemy Reset
        GameObject[] allEnemy = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemyGO in allEnemy)
        {
            //Get the base class that all enemies have
            EnemyGeneric en = enemyGO.GetComponent<EnemyGeneric>();
            if(en != null)
                en.ResetUnit();
        }

        //Asteroid Reset
        GameObject[] allAsteroid = GameObject.FindGameObjectsWithTag("Asteroid");
        foreach (var asteroidGO in allAsteroid)
        {
            Asteroid ast = asteroidGO.GetComponent<Asteroid>();
            if (ast != null)
                ast.ResetAsteroid();
        }

        //Health and Hostage Pickables Reset
        GameObject[] allPickable = GameObject.FindGameObjectsWithTag("Pickable");
        foreach (var pickGO in allPickable)
        {
            Pickable pi = pickGO.GetComponent<Pickable>();
            if (pi != null)
                pi.ResetPickable();
        }

        //ResetAllTurns();

        //Reset other variables
        hostageManager.ResetHostageCount();
        countdown = 3;
        player.enableMovement = false;
        enablePause = true;

        //After resetting all variable and elements, restart the game with the countdown
        StartCountdown();
    }

    public void ResetAllTurns()
    {
        //Level turns Reset
        GameObject[] allTurns = GameObject.FindGameObjectsWithTag("Turn");
        foreach (var turnGO in allTurns)
        {
            ShipTurnTrigger sT = turnGO.GetComponent<ShipTurnTrigger>();
            if (sT != null)
                sT.ResetTurn();
        }
    }

    public void UpdateOptions()
    {
        gameOptions.LoadVariables();
        if (player != null)
        {
            player.invertedVertical = gameOptions.controlInvertVertical;
            player.invertedHorizontal = gameOptions.controlInvertHorizontal;
        }
        if (audioManager != null)
        {
            audioManager.audioSource.volume = gameOptions.musicVolume;
            audioManager.fxSource.volume = gameOptions.fxVolume;
        }
    }

    public void EndLevel()
    {
        //end level
        enablePause = false;
        Time.timeScale = 0.0f;
        Cursor.visible = true;
        isPaused = true;
        if (hostageManager.currentHostage >= hostageManager.necessaryHostageNum)
        {
            uiManager.ShowYouWon(true);
        }
        else
        {
            uiManager.ShowYouFailed(true);
        }
    }
}
