﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostageManager : MonoBehaviour
{
    public int currentHostage;
    public int necessaryHostageNum;
    int maxHostageNumber;

    GameManager gameManager;
    UIManager uiManager;

    void Start()
    {
        gameManager = GetComponentInParent<GameManager>();
        uiManager = gameManager.uiManager;

        maxHostageNumber = 0;
        GameObject[] allPickables = GameObject.FindGameObjectsWithTag("Pickable");
        foreach(var gO in allPickables)
        {
            Pickable pick = gO.GetComponent<Pickable>();
            if(pick != null)
            {
                if (pick.category == PickableClass.Hostage)
                    maxHostageNumber++;
            }
        }        

        ResetHostageCount();

        UpdateUI();
    }

    public void RescueHostage(int amount)
    {
        currentHostage += amount;
        UpdateUI();
    }

    void UpdateUI()
    {
        uiManager.SetCurrentHostageAmount(currentHostage);
        uiManager.SetMaxHostageAmount(maxHostageNumber);
        uiManager.SetGoalHostageAmount(necessaryHostageNum);
    }

    public void ResetHostageCount()
    {
        currentHostage = 0;
        UpdateUI();
    }
}
