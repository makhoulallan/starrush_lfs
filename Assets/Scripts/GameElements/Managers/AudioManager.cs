﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioSource fxSource;

    [Header("Clips")]
    public AudioClip levelMusic;

    GameManager gameManager;

    void Start()
    {
        gameManager = GetComponentInParent<GameManager>();

        if (levelMusic != null)
        {
            audioSource.clip = levelMusic;
            audioSource.loop = true;
            audioSource.Play();
        }
    }

    public void PauseMusic(bool value)
    {
        if(audioSource != null)
        {
            if (value)
                audioSource.Pause();
            else
                audioSource.UnPause();
        }
    }

    public void PlayOneShot(AudioClip clip, float volume = 1.0f)
    {
        if(clip != null)
            fxSource.PlayOneShot(clip, volume * gameManager.gameOptions.fxVolume);
    }
}
