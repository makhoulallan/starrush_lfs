﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuTitle : MonoBehaviour
{
    public bool showMainMenuFirst = true;
    public bool showCredits = false;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
