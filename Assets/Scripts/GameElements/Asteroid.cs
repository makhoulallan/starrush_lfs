﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    public GameObject[] allAsteroids;
    public bool randomDirection = false;
    public Vector3 movementDirection;
    public bool randomSpeed = false;
    public float speed = 10.0f;
    public bool randomScale = false;
    public int maxHealth = 1;
    public int currentHealth;
    public GameObject destroyFx;
    public float destroyFxTime = 2.0f;

    Rigidbody rB;
    MeshCollider meshCol;

    public bool showMesh = true;
    [Range(0,100)]
    public float movDirectionLineScale = 40.0f;

    void Start()
    {
        rB = GetComponent<Rigidbody>();
        meshCol = GetComponent<MeshCollider>();

        SpawnAsteroid();
        ResetAsteroid();

        string time = System.DateTime.Now.ToString();
        int seed = time.GetHashCode() * gameObject.name.GetHashCode();
        if (randomDirection)
        {
            Random.InitState(seed * 65);
            movementDirection = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        }

        if(randomSpeed)
        {
            Random.InitState(seed * 95);
            speed = Random.Range(0.0f, speed);
        }

        if(randomScale)
        {
            Random.InitState(seed * 54);
            float currentScale = transform.localScale.x;
            float scaleProp = 0.2f; //Percentual to change scale
            currentScale *= Random.Range((1f - scaleProp), (1f + scaleProp));
            transform.localScale = new Vector3(currentScale, currentScale, currentScale);
        }

        movementDirection = movementDirection.normalized;
    }

    void FixedUpdate()
    {
        Move();
        Rotate();
    }

    void SpawnAsteroid()
    {
        if (allAsteroids.Length > 0)
        {
            Random.InitState(gameObject.name.GetHashCode());
            int random = Random.Range(0, allAsteroids.Length);
            GameObject ast = Instantiate(allAsteroids[random], transform);
            meshCol.sharedMesh = ast.GetComponentInChildren<MeshFilter>().sharedMesh;
            meshCol.convex = true;
            meshCol.isTrigger = false;
        }
    }

    void Move()
    {
        if (rB.velocity.magnitude < speed)
        {
            rB.AddForce(movementDirection * speed, ForceMode.Acceleration);
        }        
    }

    void Rotate()
    {
        rB.AddTorque(movementDirection * speed, ForceMode.Acceleration);
        rB.maxAngularVelocity = speed * 0.1f;
    }

    public void Hit(int damage)
    {
        if (currentHealth > 0)
        {
            currentHealth -= damage;
            if (currentHealth <= 0)
            {
                currentHealth = 0;
                DestroyAsteroid();
            }
        }
    }

    public void ResetAsteroid()
    {
        currentHealth = maxHealth;
        meshCol.enabled = true;
        if (transform.childCount > 0)
        {
            transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Asteroid"))
        {
            ContactPoint cP = collision.GetContact(0);
            movementDirection = Vector3.Reflect(movementDirection, cP.normal).normalized;
        }
            if (collision.gameObject.CompareTag("Player"))
        {
            ShipUnit sU = collision.gameObject.GetComponentInParent<ShipUnit>();
            sU.Hit(1);
            DestroyAsteroid();
        }

        if (collision.gameObject.CompareTag("Enemy"))
        {
            EnemyGeneric en = collision.gameObject.GetComponentInParent<EnemyGeneric>();
            en.Hit(1);
            DestroyAsteroid();
        }
    }

    void DestroyAsteroid()
    {
        if (destroyFx != null)
        {
            GameObject b = Instantiate(destroyFx, transform.position, transform.rotation);
            Destroy(b, destroyFxTime);
        }
        if (transform.childCount > 0)
        {
            transform.GetChild(0).gameObject.SetActive(false);
        }
        meshCol.enabled = false;
    }

    private void OnDrawGizmos()
    {
        if (showMesh)
        {
            if (!randomDirection)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, transform.position + movementDirection * movDirectionLineScale);
            }

            if (allAsteroids.Length > 0)
            {
                GameObject a1 = allAsteroids[0];
                Gizmos.color = Color.green;
                Gizmos.DrawWireMesh(a1.GetComponentInChildren<MeshFilter>().sharedMesh, transform.position);
            }
        }
    }
}
