﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipTurnTrigger : MonoBehaviour
{
    ShipController ship;
    public GameObject triggerEnter, triggerExit;
    public bool timedTrigger = false;
    public float timeToRotate = 0.5f;

    float timeRotating = 0;

    Vector3 startDirection, endDirection;
    float lerpAmount, totalDistance, shipDistance;
    bool rotating = false;
    Vector3 triggerDirection, shipPosition;

    GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        ship = gameManager.player;
    }

    void Update()
    {
        if (rotating)
        {
            if (!timedTrigger)
            {
                shipPosition = ship.transform.position - triggerEnter.transform.position;
                shipDistance = Vector3.Project(shipPosition, triggerDirection).magnitude;

                lerpAmount = Mathf.Clamp(shipDistance / totalDistance, 0.0f, 1.0f);
            }
            else
            {
                timeRotating += Time.deltaTime;
                lerpAmount = timeRotating / timeToRotate;

                if (timeRotating >= timeToRotate)
                    rotating = false;
            }

            if (lerpAmount > 1.0f)
                lerpAmount = 1.0f;

            Vector3 rotationToApply = Vector3.Slerp(startDirection, endDirection, lerpAmount);
            RotateShip(rotationToApply);
        }
    }

    public void StartTurn()
    {
        //startDirection = triggerEnter.transform.forward;
        startDirection = ship.transform.forward;
        endDirection = triggerExit.transform.forward;
        triggerDirection = triggerExit.transform.position - triggerEnter.transform.position;
        totalDistance = triggerDirection.magnitude;

        lerpAmount = 0;
        rotating = true;
        timeRotating = 0;
    }

    public void ResetTurn()
    {
        rotating = false;
    }

    public void EndTurn()
    {
        if(!timedTrigger)
            rotating = false;
    }

    void RotateShip(Vector3 direction)
    {
        ship.transform.LookAt(ship.transform.position + direction);
    }
}
