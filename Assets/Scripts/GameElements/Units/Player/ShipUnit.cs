﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipUnit : MonoBehaviour
{
    [Header("Health")]
    public int maxHealth = 5;
    public int currentHealth;
    public bool isAlive = true;

    [Header("Invicibility")]
    public float invincibleTime = 1.0f;
    float iCountTimer = 0;
    public bool isInvincible = false;
    public GameObject invincibleFx;
    GameObject invincibleGO;

    [Header("On Hit")]
    public GameObject onHitFx;
    public float destroyTimeOnHit = 2.0f;
    public AudioClip hitAudio;
    [Range(0.0f, 1.0f)]
    public float hitVolume = 1.0f;

    [Header("On Die")]
    public GameObject onDieFx;
    public float destroyTimeOnDie = 2.0f;
    public AudioClip deathAudio;
    [Range(0.0f, 1.0f)]
    public float deathVolume = 1.0f;

    [Header("Debug")]
    public bool hitTrigger = false;

    GameManager gameManager;
    UIManager uiManager;
    ShipController shipController;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        shipController = GetComponent<ShipController>();
        uiManager = gameManager.uiManager;

        currentHealth = maxHealth;

        UpdateUI();
    }

    void Update()
    {
        if(isInvincible)
        {
            iCountTimer += Time.deltaTime;
            if(iCountTimer >= invincibleTime)
            {
                SetInvincible(false);
            }
        }

        if(hitTrigger)
        {
            hitTrigger = false;
            Hit(1);
        }
    }

    public void Hit(int damage)
    {
        if(!isInvincible)
        {
            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                gameManager.audioManager.PlayOneShot(deathAudio, deathVolume);
                shipController.StopPlayer();
                currentHealth = 0;
                isAlive = false;
                if (onDieFx != null)
                {
                    shipController.mesh.SetActive(false);
                    gameManager.ResetAllTurns();
                    Destroy(Instantiate(onDieFx, transform.position, transform.rotation, transform), destroyTimeOnDie);
                    StartCoroutine(WaitDeathFX(destroyTimeOnDie));
                }
                else
                {
                    gameManager.PlayerDied(GetComponent<ShipController>());
                }
            }
            else
            {
                gameManager.audioManager.PlayOneShot(hitAudio, hitVolume);
                SetInvincible(true);
                
                if(onHitFx != null)
                    Destroy(Instantiate(onHitFx, transform.position, transform.rotation, transform), destroyTimeOnHit);
            }
            UpdateUI();
        }
    }

    IEnumerator WaitDeathFX(float time)
    {
        yield return new WaitForSeconds(time);
        gameManager.PlayerDied(GetComponent<ShipController>());
    }

    public void Heal(int amount)
    {
        currentHealth += amount;
        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
        UpdateUI();
    }

    void SetInvincible(bool value)
    {
        if(value)
        {
            iCountTimer = 0;
            isInvincible = true;
            if(invincibleFx!=null)
            {
                invincibleGO = Instantiate(invincibleFx, transform);
                invincibleGO.transform.localPosition = Vector3.zero;
            }
        }
        else
        {
            isInvincible = false;
            if(invincibleGO != null)
            {
                Destroy(invincibleGO);
            }
        }
    }

    public void ResetUnit()
    {
        currentHealth = maxHealth;
        isInvincible = false;
        isAlive = true;
        UpdateUI();
    }

    public void UpdateUI()
    {
        uiManager.SetPlayerMaxHealthUI(maxHealth);
        uiManager.SetPlayerHealthUI(currentHealth);
    }
}
