﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;
using UnityEngine.Experimental.VFX;

public class ShipController : MonoBehaviour
{
    public bool enableMovement = true;
    public bool enableInputMov = true;
    public float forSpeed = 15.0f;
    public float movSpeed = 75.0f;
    public float forDeltaSpeed = 45.0f;
    public float posLimit = 1.5f;
    public float negLimit = -3.0f;
    public GameObject mesh;
    public MovementLogic movementLogic = MovementLogic.RBForce;

    public enum MovementLogic { Position, RBForce };

    [Header("Preferences")]
    public bool invertedVertical = false;
    public bool invertedHorizontal = false;

    VisualEffect speedLine;

    [Header("Strafe")]
    public bool enableStrafe = true;
    public float strafeDistance = 5.0f;
    float strafeActualDistance;
    public float strafeSpeed = 20.0f;
    public float strafeForce = 500.0f;
    Vector3 initialStrafePosition;
    Vector3 strafeDir;
    public float strafeCooldownTime = 1.0f;
    float strafeTimer;
    bool strafeCooldown = false;
    public MovementLogic strafeLogic = MovementLogic.RBForce;
    public LayerMask wallLayer;
    [HideInInspector] public bool isStrafeing = false;
    public AudioClip strafeSound;
    [Range(0.0f, 1.0f)]
    public float strafeVolume = 1.0f;

    [Header("Boost")]
    public bool enableBoost = true;
    [Tooltip("By how much the maximum speed will be multiplied")]
    public float boostMultiplier = 1.5f;
    public float boostDuration = 2.0f;
    public float boostCooldownTime = 4.0f;
    bool boostReady = true;
    bool isBoosting = false;
    bool isBoostCooldown = false;
    float boostTimer = 0.0f;
    [Tooltip("Creates when boost start, destroys when it ends")]
    public GameObject boostFx;
    GameObject boostFxCreated;
    public AudioClip boostSound;
    [Range(0.0f, 1.0f)]
    public float boostVolume = 1.0f;

    GameManager gameManager;
    Rigidbody rB;
    ShipAnimation shipAnimation;
    ShipUnit shipUnit;
    Vector2 animInput;
    float animDelta = 0.05f;
    [HideInInspector] public UnitShoot unitShoot;
    [HideInInspector] public bool unitShootReady = false;
    Transform cameraTransform;

    //INPUT VARIABLES
    Vector2 movInput, rStickInput;
    PlayerInput pInput;
    InputAction shootAction;

    [Header("Collision")]
    public CollisionVariables obstacleCollision;
    public CollisionVariables enemyCollision;
    public CollisionVariables wallCollision;

    [System.Serializable]
    public class CollisionVariables
    {
        public bool pushBack = true;
        public bool pushBackNormalDir = false;
        public bool damage = false;
        public int damageValue = 1;
        public bool goThrough = true;
        public float pushForce = 500;
        public AudioClip hitSound;
        [Range(0.0f, 1.0f)]
        public float hitVolume = 1.0f;
    }

    void Start()
    {
        enableMovement = false;

        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        rB = GetComponent<Rigidbody>();
        pInput = GetComponent<PlayerInput>();
        shipAnimation = GetComponent<ShipAnimation>();
        shootAction = pInput.actions.FindAction("Gameplay/Shoot");
        shipUnit = GetComponent<ShipUnit>();
        unitShoot = GetComponent<UnitShoot>();
        if (unitShoot != null)
            unitShootReady = true;

        cameraTransform = Camera.main.gameObject.transform;

        speedLine = Camera.main.gameObject.GetComponentInChildren<VisualEffect>();
    }

    void FixedUpdate()
    {
        if (enableMovement)
        {
            //SHIP MOVEMENT
            ForwardMovement();

            if (enableInputMov)
                ShipMovement();

            //DASH
            float dT = Time.fixedDeltaTime;
            if (isStrafeing)
            {
                //MAYBE CHANGE DASH TO FORCE ON RIGIDBODY AS WELL
                //CALCULATE DISTANCE AND APPLY HIGH FORCE TO BE FAST
                if (strafeLogic == MovementLogic.Position)
                {
                    Vector3 pos = transform.position;
                    pos += (dT * strafeSpeed * strafeDir);
                    transform.position = pos;
                    if (Vector3.Distance(initialStrafePosition, pos) >= strafeActualDistance)
                    {
                        isStrafeing = false;
                        enableInputMov = true;
                        strafeCooldown = true;
                    }
                }
                else if (strafeLogic == MovementLogic.RBForce)
                {
                    rB.AddForce(strafeForce * strafeDir, ForceMode.Impulse);
                    isStrafeing = false;
                    enableInputMov = true;
                    strafeCooldown = true;
                }
            }
        }
    }

    void Update()
    {
        //Update opacity for speed lines
        if (speedLine != null)
        {
            float opValue;
            float vel = rB.velocity.magnitude;
            opValue = vel / forSpeed;
            if (opValue < 0)
                opValue = 0.0f;
            else if (opValue > 1.0f)
                opValue = 1.0f;
            speedLine.SetFloat("Opacity", opValue);
        }

        if (enableMovement)
        {
            //SHOOT + BULLET TIMER
            if (shootAction.phase == InputActionPhase.Started)
            {
                if (unitShoot.enableShoot)
                    unitShoot.Shoot(mesh);
            }

            //UPDATE ANIMATION
            if (Mathf.Abs(animInput.x - movInput.x) < animDelta)
            {
                animInput.x = movInput.x;
            }
            else if (animInput.x < movInput.x)
            {
                animInput.x += animDelta;
            }
            else if (animInput.x > movInput.x)
            {
                animInput.x -= animDelta;
            }
            if (Mathf.Abs(animInput.y - movInput.y) < animDelta)
            {
                animInput.y = movInput.y;
            }
            else if (animInput.y < movInput.y)
            {
                animInput.y += animDelta;
            }
            else if (animInput.y > movInput.y)
            {
                animInput.y -= animDelta;
            }

            shipAnimation.hInput = animInput.x;
            shipAnimation.vInput = animInput.y;
        }

        float dT = Time.deltaTime;
        //STRAFE
        if (strafeCooldown)
        {
            strafeTimer += dT;
            if (strafeTimer >= strafeCooldownTime)
            {
                strafeCooldown = false;
            }
        }

        //BOOST
        if (isBoosting)
        {
            boostTimer += dT;
            if (boostTimer >= boostDuration)
            {
                isBoosting = false;
                isBoostCooldown = true;
                boostTimer = 0.0f;
                if (boostFxCreated != null)
                    Destroy(boostFxCreated);
            }
        }

        if (isBoostCooldown)
        {
            boostTimer += dT;
            if (boostTimer >= boostCooldownTime)
            {
                boostReady = true;
                isBoostCooldown = false;
            }
        }

        UpdateUI();
    }

    void ForwardMovement()
    {
        if (movementLogic == MovementLogic.Position)
        {
            float dT = Time.deltaTime;
            Vector3 pos = transform.position;
            float maxSpeed = isBoosting ? forSpeed * boostMultiplier : forSpeed;
            pos += (maxSpeed * dT * transform.forward);
            transform.position = pos;
        }
        else if (movementLogic == MovementLogic.RBForce)
        {
            float sp = Vector3.Project(rB.velocity, transform.forward).magnitude;
            float maxSpeed = isBoosting ? forSpeed * boostMultiplier : forSpeed;
            float forceToApply = 1000;
            float forceValue = isBoosting ? forceToApply * boostMultiplier : forceToApply;
            if (sp < maxSpeed)
            {
                rB.AddForce(forceValue * transform.forward, ForceMode.Acceleration);
            }
        }
    }

    void ShipMovement()
    {
        float dT = Time.deltaTime;

        rB.AddForce(GetMovDirection() * movSpeed, ForceMode.Force);

        //RIGHT STICK INPUT
        //NOT BEING USED ANYMORE
        /*
        Vector3 meshPos = mesh.transform.localPosition;
        meshPos.z += (forDeltaSpeed * dT * rStickInput.y);
        meshPos.z = Mathf.Clamp(meshPos.z, negLimit, posLimit);
        mesh.transform.localPosition = meshPos;
        */
    }

    Vector3 GetMovDirection()
    {
        Vector3 rightForce = transform.right * movInput.x;
        Vector3 upForce = transform.up * movInput.y;
        Vector3 dirForce = (rightForce + upForce).normalized;
        return dirForce;
    }

    void Strafe()
    {
        if (movInput.magnitude > 0.1f && !strafeCooldown && !isStrafeing && enableStrafe)
        {
            strafeCooldown = false;
            isStrafeing = true;
            enableInputMov = false;
            strafeTimer = 0;
            initialStrafePosition = transform.position;
            strafeDir = GetMovDirection();
            gameManager.audioManager.PlayOneShot(strafeSound, strafeVolume);

            //Raycast for obstacles
            float strafeOffset = 12;
            if (Physics.Raycast(transform.position, strafeDir, out RaycastHit rayOut, strafeDistance, wallLayer))
            {
                strafeActualDistance = Mathf.Clamp(rayOut.distance - strafeOffset, 0, strafeDistance);
                if (rayOut.distance < 16)
                {
                    isStrafeing = false;
                    enableInputMov = true;
                    strafeCooldown = true;
                }
            }
            else
            {
                strafeActualDistance = strafeDistance;
            }
        }
    }

    void PushBack(float pushForce, Vector3? pushBackDirection = null)
    {
        Vector3 pushDir = pushBackDirection == null ? -transform.forward : (Vector3)pushBackDirection;
        rB.AddForce(pushForce * pushDir, ForceMode.Impulse);
    }

    public void OnMove(InputValue value)
    {
        movInput = value.Get<Vector2>();
        if (invertedHorizontal)
            movInput.x *= -1;
        if (invertedVertical)
            movInput.y *= -1;
    }

    public void OnStrafe()
    {
        if (enableMovement && enableInputMov)
            Strafe();
    }

    public void OnBoost()
    {
        if (enableMovement && enableInputMov)
            if (enableBoost && boostReady)
            {
                boostReady = false;
                boostTimer = 0.0f;
                isBoosting = true;

                gameManager.audioManager.PlayOneShot(boostSound, boostVolume);

                if (boostFx != null)
                {
                    boostFxCreated = Instantiate(boostFx, cameraTransform);
                    //boostFxCreated.transform.localPosition = Vector3.zero;
                }
            }
    }

    public void OnRStick(InputValue value)
    {
        rStickInput = value.Get<Vector2>();
    }

    public void UpdateUI()
    {
        gameManager.uiManager.SetMaxStrafe(1.0f);
        gameManager.uiManager.SetMaxBoost(1.0f);

        if (unitShoot.hasCooldown)
        {
            gameManager.uiManager.SetShotSliderVisible(true);
            gameManager.uiManager.SetShootSlider(unitShoot.GetShootRateValue());
        }
        else
        {
            gameManager.uiManager.SetShotSliderVisible(false); 
        }

        if (enableStrafe && !strafeCooldown && !isStrafeing)
        {
            gameManager.uiManager.SetStrafeBar(1.0f);
        }
        else if (enableStrafe && isStrafeing)
        {
            gameManager.uiManager.SetStrafeBar(0.0f);
        }
        else if (enableStrafe && strafeCooldown)
        {
            float barValue = Mathf.Clamp(strafeTimer / strafeCooldownTime, 0.0f, 1.0f);
            gameManager.uiManager.SetStrafeBar(barValue);
        }
        else if (!enableStrafe)
        {
            gameManager.uiManager.SetStrafeBar(0.0f);
        }

        if (enableBoost && !isBoosting && !isBoostCooldown)
        {
            gameManager.uiManager.SetBoostBar(1.0f);
        }
        else if (enableBoost && isBoosting)
        {
            float barValue = Mathf.Clamp(boostTimer / boostDuration, 0.0f, 1.0f);
            gameManager.uiManager.SetBoostBar(1 - barValue);
        }
        else if (enableBoost && isBoostCooldown)
        {
            float barValue = Mathf.Clamp(boostTimer / boostCooldownTime, 0.0f, 1.0f);
            gameManager.uiManager.SetBoostBar(barValue);
        }
        else if (!enableBoost)
        {
            gameManager.uiManager.SetBoostBar(0.0f);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        string collisionTag = collision.gameObject.tag;
        switch (collisionTag)
        {
            case "Wall":
                rB.velocity = Vector3.Project(rB.velocity, transform.forward);
                if (isStrafeing)
                {
                    isStrafeing = false;
                    enableInputMov = true;
                    strafeCooldown = true;
                }
                CollisionLogic(wallCollision, collision);
                break;
            case "Obstacle":
                CollisionLogic(obstacleCollision, collision);
                break;
            case "Enemy":
                CollisionLogic(enemyCollision, collision);
                EnemyGeneric en = collision.gameObject.GetComponentInParent<EnemyGeneric>();
                if (enemyCollision.damage)
                {
                    en.Hit(1);
                }
                break;
        }
    }

    void CollisionLogic(CollisionVariables cV, Collision cl)
    {
        if (cV.pushBack)
        {
            if (cV.pushBackNormalDir)
            {
                PushBack(cV.pushForce, cl.contacts[0].normal);
            }
            else
            {
                PushBack(cV.pushForce);
            }
        }

        if (cV.damage)
        {
            shipUnit.Hit(cV.damageValue);
        }

        if (cV.goThrough)
        {
            ContactPoint cP = cl.GetContact(0);
            Physics.IgnoreCollision(cP.thisCollider, cP.otherCollider, true);
            StartCoroutine(ResetCollision(cP.thisCollider, cP.otherCollider, 2.0f));
        }

        gameManager.audioManager.PlayOneShot(cV.hitSound, cV.hitVolume);
    }

    IEnumerator ResetCollision(Collider c1, Collider c2, float time)
    {
        yield return new WaitForSeconds(time);
        Physics.IgnoreCollision(c1, c2, false);
    }

    public void StopPlayer()
    {
        rB.velocity = Vector3.zero;
        rB.angularVelocity = Vector3.zero;
    }

    public void ResetController()
    {
        StopPlayer();
        isStrafeing = false;
        isBoosting = false;
        strafeCooldown = false;
        isBoostCooldown = false;
        boostReady = true;
        mesh.SetActive(true);
        unitShoot.ResetShoot();
        if (boostFxCreated != null)
            Destroy(boostFxCreated);
        UpdateUI();
    }
}