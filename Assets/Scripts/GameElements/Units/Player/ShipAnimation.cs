﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAnimation : MonoBehaviour
{
    Animator shipAnimator;
    [HideInInspector] public float hInput, vInput;

    void Start()
    {
        shipAnimator = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        shipAnimator.SetFloat("Hor_Speed", hInput);
        shipAnimator.SetFloat("Ver_Speed", vInput);
    }
}
