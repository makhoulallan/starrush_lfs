﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Droppable : MonoBehaviour
{
    public GameObject[] allDrops;

    [Range(0,100)]
    public float chanceToDropSomething = 50;

    public void Drop()
    {
        if (allDrops.Length > 0)
        {
            System.DateTime dateTime = System.DateTime.Now;
            int seed = dateTime.ToString().GetHashCode() * gameObject.name.GetHashCode();
            Random.InitState(seed);

            float chance = Random.Range(0, 100);
            if (chance <= chanceToDropSomething)
            {
                seed *= Mathf.CeilToInt(chance);
                Random.InitState(seed);

                int index = Random.Range(0, allDrops.Length);
                GameObject objToDrop = allDrops[index];

                GameObject dropped = Instantiate(objToDrop, transform.position, transform.rotation);
            }
        }
    }
}
