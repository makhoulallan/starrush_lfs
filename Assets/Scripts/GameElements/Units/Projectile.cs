﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float bulletSpeed = 20.0f;
    public float bulletMaxDistance = 100.0f;
    public int damage = 1;
    
    public GameObject fx;

    string originTag;
    Vector3 endPosition;
    GameObject originObject, targetObject;
    ShootDirection shootDirection;

    Vector3 initialPosition, movDir;
    bool ready = false;

    void Start()
    {
        initialPosition = transform.position;

        switch (shootDirection)
        {
            case ShootDirection.Center:
                endPosition = originObject.transform.position + bulletMaxDistance * transform.forward;
                break;
            case ShootDirection.Straight:
                endPosition = initialPosition + bulletMaxDistance * transform.forward;
                break;
        }
    }
    Vector3 dirTest;
    void Update()
    {
        if (ready)
        {
            float dT = Time.deltaTime;

            Vector3 pos = transform.position;

            pos += (bulletSpeed * dT * movDir);
            transform.position = pos;

            if (Vector3.Distance(pos, initialPosition) >= bulletMaxDistance)
            {
                DestroyBullet();
            }
        }
    }

    public void SetVariables(int dmg, string orgTag, ShootDirection shDir, GameObject orgObject, GameObject target = null)
    {
        initialPosition = transform.position;

        damage = dmg;
        originTag = orgTag;
        shootDirection = shDir;
        originObject = orgObject;
        targetObject = target;

        if(targetObject != null)
        {
            endPosition = targetObject.transform.position;
        }
        else
        {
            switch (shootDirection)
            {
                case ShootDirection.Center:
                    endPosition = originObject.transform.position + bulletMaxDistance * transform.forward;
                    break;
                case ShootDirection.Straight:
                    endPosition = initialPosition + bulletMaxDistance * transform.forward;
                    break;
            }
        }
        movDir = (endPosition - initialPosition).normalized;
        transform.LookAt(transform.position + movDir);
        ready = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        switch(other.tag)
        {
            case "Player":
                if (originTag != "Player")
                {
                    ShipUnit sU = other.gameObject.GetComponentInParent<ShipUnit>();
                    sU.Hit(damage);
                    DestroyBullet();
                }
                break;
            case "Enemy":
                if (originTag != "Enemy")
                {
                    EnemyGeneric en = other.gameObject.GetComponentInParent<EnemyGeneric>();
                    en.Hit(damage);
                    DestroyBullet();
                }
                break;
            case "Asteroid":
                Asteroid ast = other.gameObject.GetComponentInParent<Asteroid>();
                ast.Hit(damage);
                DestroyBullet();
                break;
            default:
                DestroyBullet();
                break;
        }        
    }

    void DestroyBullet()
    {
        Destroy(gameObject);
    }
}