﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShootStyle {AllPositions, RoundRobin };
public enum ShootDirection { Straight, Center};

public class UnitShoot : MonoBehaviour
{
    [Header("Shoot")]
    public bool enableShoot = true;
    public float fireRate = 15;
    public ShootStyle shootStyle = ShootStyle.AllPositions;
    public ShootDirection shootDirection = ShootDirection.Straight;

    [Header("Cooldown")]
    public bool hasCooldown = false;
    public float shootingRate = 4.0f;
    public float recoverRate = 2.0f;
    float maxRate = 100.0f;
    float currentRate;
    bool canShootRate = true;
    bool frameShot = false;
    public AudioClip overflowAudio;
    [Range(0.0f, 1.0f)]
    public float overflowVolume = 1.0f;
    public AudioClip recoveredAudio;
    [Range(0.0f, 1.0f)]
    public float recoveredVolume = 1.0f;
    public GameObject overflowFx;
    public float overflowFxTime = 2.0f;

    [Header("Bullet")]
    public int bulletDamage = 1;
    public GameObject bulletPrefab;
    public Transform[] bulletSpawnPosition;
    
    bool canShoot = true;
    float bulletFireRateTime;
    float bulletTimer = 0;
    bool bulletCooldown = false;
    int spawnPosition = 0;

    [Header("Audio")]
    public AudioClip shootAudio;
    [Range(0.0f, 1.0f)]
    public float shootVolume = 1.0f;
    [Range(0.0f, 2.0f)]
    public float timeBetweenAudio = 0.0f;
    bool canPlayAudio = true;

    [Header("Debug")]
    public bool alwaysShoot = false;

    GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        currentRate = maxRate;
    }

    void Update()
    {
        float dT = Time.deltaTime;

        if (bulletCooldown)
        {
            bulletTimer += dT;
            if (bulletTimer >= bulletFireRateTime)
            {
                bulletCooldown = false;
                canShoot = true;
            }
        }

        if(alwaysShoot)
        {
            Shoot(gameObject);
        }

        //CALCULATE RATE
        if (hasCooldown)
        {
            currentRate += (recoverRate * dT);
            if(frameShot && canShootRate)
            {
                currentRate -= (shootingRate * dT);
                frameShot = false;
            }

            if (currentRate < 0.0f)
                currentRate = 0.0f;
            if (currentRate > maxRate)
                currentRate = maxRate;

            if (currentRate == 0)
            {
                canShootRate = false;
                gameManager.audioManager.PlayOneShot(overflowAudio, overflowVolume);

                if(overflowFx != null)
                    Destroy(Instantiate(overflowFx), overflowFxTime);
            }

            if (!canShootRate && currentRate == maxRate)
            {
                canShootRate = true;
                gameManager.audioManager.PlayOneShot(recoveredAudio, recoveredVolume);
            }
        }
        
    }

    public void Shoot(GameObject mesh, GameObject playerTarget = null)
    {
        frameShot = true;

        if (!hasCooldown || (hasCooldown && canShootRate))
        {
            bulletFireRateTime = 1 / fireRate;
            if (canShoot)
            {
                canShoot = false;
                bulletTimer = 0;
                List<Transform> positionToSpawn = new List<Transform>();

                if (bulletSpawnPosition.Length > 0)
                {
                    switch (shootStyle)
                    {
                        case ShootStyle.AllPositions:
                            foreach (var pos in bulletSpawnPosition)
                            {
                                positionToSpawn.Add(pos);
                            }
                            break;
                        case ShootStyle.RoundRobin:
                            positionToSpawn.Add(bulletSpawnPosition[spawnPosition]);
                            spawnPosition++;
                            spawnPosition %= bulletSpawnPosition.Length;
                            break;
                    }
                }
                else
                {
                    positionToSpawn.Add(mesh.transform);
                }

                List<Projectile> bul = new List<Projectile>();

                if (positionToSpawn.Count > 0)
                {
                    foreach (var pos in positionToSpawn)
                    {
                        Projectile p = Instantiate(bulletPrefab, pos.position, pos.rotation).GetComponent<Projectile>();
                        bul.Add(p);
                    }

                    foreach (var b in bul)
                    {
                        b.SetVariables(bulletDamage, gameObject.tag, shootDirection, gameObject, playerTarget);
                    }
                }

                bul.Clear();
                bul.TrimExcess();
                bulletCooldown = true;

                if (canPlayAudio)
                {
                    gameManager.audioManager.PlayOneShot(shootAudio, shootVolume);
                    StartCoroutine(WaitAfterShot());
                }
            }
        }
    }

    IEnumerator WaitAfterShot()
    {
        canPlayAudio = false;
        yield return new WaitForSeconds(timeBetweenAudio);
        canPlayAudio = true;
    }

    public float GetShootRateValue()
    {
        if(hasCooldown)
        {
            return currentRate / maxRate;
        }
        else
        {
            return 1.0f;
        }
    }

    public void ResetShoot()
    {
        currentRate = maxRate;
        bulletCooldown = false;
        canShoot = true;
        frameShot = false;
    }
}