﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserReticle : MonoBehaviour
{
    ShipController ship;
    public bool liveUpdateReticle = false;
    public bool followShipRotation = false;
    [Range(0,1)]
    public float reticlePosition = 0.8f;
    RectTransform rTransform;
    Camera mainCamera;

    GameManager gameManager;

    Vector3 startPosition;
    Vector3 forDirection;
    float reticleDistance;
    bool firstTime = true;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        ship = gameManager.player;

        rTransform = GetComponent<RectTransform>();
        mainCamera = Camera.main;

        UpdateReticlePosition();
    }

    void Update()
    {
        if (ship == null)
            ship = gameManager.player;

        if (firstTime)
            UpdateReticlePosition();

        if (liveUpdateReticle)
            UpdateReticlePosition();
    }

    void UpdateReticlePosition()
    {
        if (ship.unitShootReady)
        {
            startPosition = ship.mesh.transform.position;
            forDirection = followShipRotation ? ship.mesh.transform.forward : ship.transform.forward;
            reticleDistance = ship.unitShoot.bulletPrefab.GetComponent<Projectile>().bulletMaxDistance * reticlePosition;
            Vector3 endPosition = startPosition + forDirection * reticleDistance;
            Vector3 pos = mainCamera.WorldToScreenPoint(endPosition);
            pos.z = 0;
            rTransform.position = pos;
            firstTime = false;
        }
    }
}
