﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetectionRange : MonoBehaviour
{
    float defaultZposition;
    BoxCollider bC;
    EnemyGeneric enemyScript;
    float currentDetectionRange;

    void Start()
    {
        bC = GetComponent<BoxCollider>();
        enemyScript = GetComponentInParent<EnemyGeneric>();
        defaultZposition = bC.size.z/2 + bC.center.z;
        currentDetectionRange = bC.size.z;
    }

    void Update()
    {
        if(currentDetectionRange != enemyScript.shootRange)
        {
            SetDetectionRange(enemyScript.shootRange);
        }
    }

    void SetDetectionRange(float newDetectionRange)
    {
        bC.size = new Vector3(bC.size.x, bC.size.y, newDetectionRange);
        bC.center = new Vector3(bC.center.x, bC.center.y, defaultZposition - newDetectionRange/2);
        currentDetectionRange = newDetectionRange;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            enemyScript.playerInRange = true;
            enemyScript.playerGO = other.gameObject.GetComponentInParent<ShipController>().gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            enemyScript.playerInRange = false;
            enemyScript.playerGO = null;
        }
    }
}
