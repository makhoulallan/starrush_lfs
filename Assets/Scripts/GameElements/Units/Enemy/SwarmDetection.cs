﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwarmDetection : MonoBehaviour
{
    SwarmLogic swarm;

    float defaultZposition;
    BoxCollider bC;
    float currentDetectionRange;

    void Start()
    {
        bC = GetComponent<BoxCollider>();
        swarm = GetComponentInParent<SwarmLogic>();

        defaultZposition = bC.size.z / 2 + bC.center.z;
        currentDetectionRange = bC.size.z;
    }

    void Update()
    {
        if (currentDetectionRange != swarm.shootRange)
        {
            SetDetectionRange(swarm.shootRange);
        }
    }

    void SetDetectionRange(float newDetectionRange)
    {
        bC.size = new Vector3(bC.size.x, bC.size.y, newDetectionRange);
        bC.center = new Vector3(bC.center.x, bC.center.y, defaultZposition - newDetectionRange / 2);
        currentDetectionRange = newDetectionRange;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            swarm.playerInRange = true;
            swarm.playerGO = other.gameObject.GetComponentInParent<ShipController>().gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            swarm.playerInRange = false;
            swarm.playerGO = null;
        }
    }
}
