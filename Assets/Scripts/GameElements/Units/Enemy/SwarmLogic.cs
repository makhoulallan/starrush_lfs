﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwarmLogic : MonoBehaviour
{
    public float speed = 150f;
    public Vector3 offsetRange;
    public float fireRateOffset = 0.5f;
    SmallEnemy[] allEnemies;

    [HideInInspector] public float shootRange = 0;
    [HideInInspector] public bool playerInRange = false;
    [HideInInspector] public GameObject playerGO;

    Vector3 targetPosition;
    List<Vector3> initialPositions = new List<Vector3>();

    void Start()
    {
        allEnemies = transform.GetComponentsInChildren<SmallEnemy>();

        foreach(var sE in allEnemies)
        {
            initialPositions.Add(sE.transform.localPosition);
            int s = sE.GetHashCode();
            Random.InitState(s);
            float fR = Random.Range(-fireRateOffset, fireRateOffset);
            sE.GetComponent<UnitShoot>().fireRate += fR;
        }

        UpdateShootVariables();
    }

    void Update()
    {
        UpdateShootVariables();
        
    }

    void FixedUpdate()
    {
        MoveSwarm();
        MoveEachShipInsideSwarm();
    }

    void UpdateShootVariables()
    {
        if (allEnemies.Length > 0)
        {
            shootRange = allEnemies[0].shootRange;
        }

        foreach(var sE in allEnemies)
        {
            sE.playerGO = playerGO;
            sE.playerInRange = playerInRange;
        }
    }

    void MoveSwarm()
    {
        if(playerGO != null)
        {
            targetPosition = playerGO.transform.position;
            Vector3 dir = targetPosition - transform.position;

            Vector3 upDir = Vector3.Project(dir, transform.up);
            Vector3 rightDir = Vector3.Project(dir, transform.right);
            Vector3 movDir = (upDir + rightDir);

            if(movDir.magnitude > 4f)
            {
                movDir = movDir.normalized;
                Vector3 pos = transform.position;
                pos += (movDir * speed * Time.fixedDeltaTime);
                transform.position = pos;
            }
        }
    }

    void MoveEachShipInsideSwarm()
    {
        for (int i = 0; i < allEnemies.Length; i++)
        {
            SmallEnemy sE = allEnemies[i];
            if(sE.reachedPosition)
            {
                Vector3 offset = GetOffset(sE.name);
                sE.SetNewPosition(initialPositions[i] + offset + transform.position);
            }
        }
    }

    Vector3 GetOffset(string seed)
    {
        float offX, offY, offZ;

        int s = seed.GetHashCode() + Mathf.RoundToInt(Time.realtimeSinceStartup * 53.42f);

        Random.InitState(s);

        offX = Random.Range(-offsetRange.x, offsetRange.x);
        offY = Random.Range(-offsetRange.y, offsetRange.y);
        offZ = Random.Range(-offsetRange.z, offsetRange.z);

        return new Vector3(offX, offY, offZ);
    }
}
