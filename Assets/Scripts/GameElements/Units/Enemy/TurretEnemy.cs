﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretEnemy : EnemyGeneric
{
    [Header("Turret Elements")]
    public Transform turretRotBase;
    public Transform turretCockpitBase;

    Vector3 targetPosition;

    protected override void Move()
    {
        if (playerGO != null)
        {
            //Calculate the target position and transform to local space by projecting
            targetPosition = playerGO.transform.position;
            Vector3 dir = targetPosition - transform.position;

            Vector3 forDir = Vector3.Project(dir, transform.forward);
            Vector3 rightDir = Vector3.Project(dir, transform.right);

            //Base Rotation
            Vector3 lookDirBase = transform.position + forDir + rightDir;            
            turretRotBase.LookAt(lookDirBase);
            Debug.DrawLine(transform.position, lookDirBase, Color.red);

            //Cockpit Rotation
            turretCockpitBase.LookAt(targetPosition);
        }
    }
}
