﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediumEnemy : EnemyGeneric
{
    Vector3 targetPosition;
    public Vector2 positionOffset;

    protected override void Move()
    {
        if (playerGO != null)
        {
            //Calculate the target position and transform to local space by projecting
            targetPosition = playerGO.transform.position;
            Vector3 dir = targetPosition - transform.position;

            Vector3 upDir = Vector3.Project(dir, transform.up)*(positionOffset.x+1);
            Vector3 rightDir = Vector3.Project(dir, transform.right) * (positionOffset.y+1);
            Vector3 movDir = (upDir + rightDir);

            //Check if it needs to move or the current position in within acceptable range
            /*
            if (movDir.magnitude > 4f)
            {
                movDir = movDir.normalized;
                Vector3 pos = transform.position;
                pos += (movDir * speed * Time.fixedDeltaTime);
                transform.position = pos;
            }
            */
            if(movDir.magnitude > 2f)
            {
                movDir = movDir.normalized;
                GetComponent<Rigidbody>().AddForce(speed * movDir, ForceMode.Acceleration);
            }
        }
    }
}