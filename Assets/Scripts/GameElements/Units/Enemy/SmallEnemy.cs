﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallEnemy : EnemyGeneric
{
    [HideInInspector] public Vector3 targetPosition;
    [HideInInspector] public bool reachedPosition = true;
    float lerpAmount = 0;
    Vector3 initialChildPosition;
    float speedRandom;

    protected override void Start()
    {
        base.Start();

        reachedPosition = true;
    }

    protected override void Move()
    {
        //-------SMALL ENEMY MOVEMENT SET BY SWARM LOGIC----------

        if (!reachedPosition)
        {
            float speedMin = speedRandom / 4;
            float a = 6.0f * speedMin - 4.0f * speedRandom;
            float b = 4.0f * speedRandom - 5.0f * speedMin;
            float c = speedMin;
            float speedSmooth = (a * lerpAmount * lerpAmount) + (b * lerpAmount) + (c);

            lerpAmount += (Time.fixedDeltaTime * speedSmooth);
            if (lerpAmount >= 1.0f)
            {
                lerpAmount = 1;
                reachedPosition = true;
            }

            Vector3 pos = Vector3.Lerp(initialChildPosition, targetPosition, lerpAmount);

            transform.position = pos;
        }
    }

    public void SetNewPosition(Vector3 target)
    {
        targetPosition = target;
        reachedPosition = false;
        lerpAmount = 0;

        initialChildPosition = transform.position;

        int s = Mathf.RoundToInt(Time.realtimeSinceStartup * 145.4f) + gameObject.name.GetHashCode() + 78;
        Random.InitState(s);
        speedRandom = Random.Range(speed / 1.5f, speed);
    }
}
