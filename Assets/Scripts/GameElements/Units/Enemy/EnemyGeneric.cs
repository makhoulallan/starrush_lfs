﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.VFX;

public class EnemyGeneric : MonoBehaviour
{
    public GameObject mesh;

    [Header("Movement")]
    public float speed = 5.0f;    

    [Header("Shooting")]
    public float shootRange = 100f;
    public bool playerInRange = false;
    [HideInInspector] public GameObject playerGO;

    [Header("Enemy Unit")]
    public int maxHealth = 1;
    public int currentHealth;
    public bool isAlive = true;
    public bool isInvincible = false;
    public float invincibleTime = 1.0f;
    public GameObject deathFx;
    public float timeToDestroyFx = 2.0f;
    public Material hitMaterial;
    Material[] currentMaterial;
    Collider enemyCollider;

    [Header("Audio")]
    public AudioClip hitAudio;
    [Range(0.0f, 1.0f)]
    public float hitVolume = 1.0f;
    public AudioClip deathAudio;
    [Range(0.0f, 1.0f)]
    public float deathVolume = 1.0f;

    [Header("Debug")]
    public bool hitTrigger = false;
    public bool resetTrigger = false;

    Droppable droppable;    
    UnitShoot unitShoot;
    GameManager gameManager;
    VisualEffect visualEffect;
    Rigidbody rB;

    Vector3 initialPosition;
    Quaternion initialRotation;

    protected virtual void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        enemyCollider = GetComponent<Collider>();
        droppable = GetComponent<Droppable>();
        unitShoot = GetComponent<UnitShoot>();
        visualEffect = GetComponentInChildren<VisualEffect>();
        rB = GetComponent<Rigidbody>();

        MeshRenderer[] allMeshRend = GetComponentsInChildren<MeshRenderer>();
        currentMaterial = new Material[allMeshRend.Length];
        for (int i = 0; i < allMeshRend.Length; i++)
        {
            currentMaterial[i] = allMeshRend[i].material;
        }
        
        if(unitShoot == null)
        {
            Debug.LogError("Enemy needs UnitShoot!");
        }

        initialPosition = transform.position;
        initialRotation = transform.rotation;

        ResetUnit();
    }

    void Update()
    {
        if (isAlive)
        {
            
            if (playerInRange)
            {
                ShowIndicator(true);
                unitShoot.Shoot(mesh, playerGO);
            }
        }

        if(hitTrigger)
        {
            hitTrigger = false;
            Hit(1);
        }

        if(resetTrigger)
        {
            resetTrigger = false;
            ResetUnit();
        }
    }

    void FixedUpdate()
    {
        if (isAlive)
        {
            Move();
        }
    }

    protected virtual void Move()
    {
    }

    public void Hit(int damage)
    {
        if (currentHealth > 0 && !isInvincible)
        {
            currentHealth -= damage;
            if (currentHealth <= 0)
            {
                currentHealth = 0;
                gameManager.audioManager.PlayOneShot(deathAudio, deathVolume);
                Die();
            }
            else
            {
                gameManager.audioManager.PlayOneShot(hitAudio, hitVolume);
                isInvincible = true;
                StartCoroutine(InvincibleTime());
            }
        }
    }

    public void ResetUnit()
    {
        currentHealth = maxHealth;
        playerInRange = false;
        playerGO = null;
        mesh.SetActive(true);
        if (enemyCollider != null)
            enemyCollider.enabled = true;
        isAlive = true;
        isInvincible = false;
        transform.position = initialPosition;
        transform.rotation = initialRotation;
        ResetRB();
        ShowIndicator(true);
    }

    void Die()
    {
        isAlive = false;

        if (enemyCollider != null)
            enemyCollider.enabled = false;
        mesh.SetActive(false);
        ShowIndicator(false);

        if (deathFx != null)
        {
            GameObject b = Instantiate(deathFx, mesh.transform.position, mesh.transform.rotation);
            Destroy(b, timeToDestroyFx);
        }
        
        if(droppable != null)
            droppable.Drop();

        ResetRB();
    }

    void ResetRB()
    {
        if(rB != null)
        {
            rB.velocity = Vector3.zero;
            rB.angularVelocity = Vector3.zero;
        }
    }

    public void ShowIndicator(bool value)
    {
        if (visualEffect != null)
        {
            if(value)
            {
                visualEffect.gameObject.SetActive(true);
                visualEffect.Play();
            }
            else
            {
                visualEffect.Stop();
                visualEffect.gameObject.SetActive(false);
            }
            
        }
    }

    IEnumerator InvincibleTime()
    {
        ChangeMaterial(true);
        yield return new WaitForSeconds(invincibleTime);
        isInvincible = false;
        ChangeMaterial(false);
    }

    public void ChangeMaterial(bool hit)
    {
        if(hitMaterial != null)
        {
        MeshRenderer[] allMeshRend = GetComponentsInChildren<MeshRenderer>();

        if (hit)
        {
            for (int i = 0; i < allMeshRend.Length; i++)
            {
                allMeshRend[i].material = hitMaterial;
            }
        }
        else
        {
            for (int i = 0; i < allMeshRend.Length; i++)
            {
                allMeshRend[i].material = currentMaterial[i];
            }
        }
        }
    }
}
