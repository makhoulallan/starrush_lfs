﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetEND : MonoBehaviour
{
    public string resetTag = "Player";

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(resetTag))
        {
            GameManager gM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
            gM.EndLevel();
        }
    }
}
