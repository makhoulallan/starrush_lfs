﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEvent : MonoBehaviour
{
    public string triggerTag = "Player";
    public bool enableOnEnter, enableOnStay, enableOnExit;
    public UnityEvent onEnter;
    public UnityEvent onStay;
    public UnityEvent onExit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(triggerTag))
        {
            if (enableOnEnter)
            {
                onEnter.Invoke();
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(triggerTag))
        {
            if (enableOnStay)
            {
                onStay.Invoke();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(triggerTag))
        {
            if (enableOnExit)
            {
                onExit.Invoke();










            }
        }
    }
}